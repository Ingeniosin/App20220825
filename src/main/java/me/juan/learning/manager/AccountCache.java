package me.juan.learning.manager;

import me.juan.learning.entity.Account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AccountCache {

    private static AccountCache instance;
    public HashMap<String, Account> accountsByUsername = new HashMap<>();
    public List<Account> accounts = new ArrayList<>();

    public Account getCloneAccountByUsername(String userName) {
        Account account = accountsByUsername.getOrDefault(userName, null);
        if (account == null)
            throw new RuntimeException("Account not found");
        return account.clone();
    }

    public void OnCreateAccount(Account account) {
        accountsByUsername.put(account.getUser().getName(), account);
        accounts.add(account);
    }

    public static AccountCache getInstance() {
        if (instance == null) {
            instance = new AccountCache();
        }
        return instance;
    }

}
