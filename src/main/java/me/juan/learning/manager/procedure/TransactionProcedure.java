package me.juan.learning.manager.procedure;

import lombok.Data;
import lombok.Getter;
import me.juan.learning.Main;
import me.juan.learning.entity.Account;
import me.juan.learning.entity.Transaction;
import me.juan.learning.manager.TransactionResponse;
import me.juan.learning.manager.transaction.TransactionManager;

@Data
public class TransactionProcedure extends Procedure<Account> {

    private double amount;

    public TransactionProcedure(Procedure<Account> prevProcedure, double amount) {
        super(prevProcedure);
        this.amount = amount;
    }

    public TransactionProcedure(double amount) {
        this.amount = amount;
    }

    @Override
    protected void execute(Account param) {
        TransactionManager manager = Main.transactionManager;
        Transaction transaction = new Transaction(param, amount);
        System.out.println("Running transaction: " + amount + " from account: " + param.getUser().getName());
        TransactionResponse<Transaction> response = manager.beginTransaction(transaction);
        if (!response.isSuccess()){
            throw new RuntimeException("Transaction failed");
        }
    }
}
