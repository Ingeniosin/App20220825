package me.juan.learning.manager.procedure;

import lombok.Data;

@Data
public abstract class Procedure<T> {

    private Procedure<T> prevProcedure;

    public Procedure(Procedure<T> prevProcedure) {
        prevProcedure.setPrevProcedure(this);
    }

    public Procedure() {
    }

    protected abstract void execute(T param);

    public void run(T param) {
        execute(param);
        if (prevProcedure != null) {
            prevProcedure.run(param);
        }
    }



}
