package me.juan.learning.manager.transaction.rules;

import me.juan.learning.entity.Transaction;
import me.juan.learning.legacy.UnusualMotionDetector;
import me.juan.learning.validator.Rule;
import me.juan.learning.validator.ValidationResponse;

public class AdapterMotionDetectorRule implements Rule<Transaction> {
    private final UnusualMotionDetector unusualMotionDetector;

    public AdapterMotionDetectorRule(UnusualMotionDetector unusualMotionDetector) {
        this.unusualMotionDetector = unusualMotionDetector;
    }

    @Override
    public String getName() {
        return "Adapter motion detector rule";
    }

    @Override
    public ValidationResponse<Transaction> validate(Transaction model, ValidationResponse<Transaction> response) {
        String result = unusualMotionDetector.checkMotion(model.getAmount());
        if (result != null) {
            response.setMessage(result);
        }

        return response;
    }
}
