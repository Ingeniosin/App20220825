package me.juan.learning.manager.transaction.rules;

import me.juan.learning.entity.Transaction;
import me.juan.learning.validator.Rule;
import me.juan.learning.validator.ValidationResponse;

import java.util.Date;

public class ActivationDateWithMax implements Rule<Transaction> {

    @Override
    public String getName() {
        return "Activation date rule with max";
    }

    @Override
    public ValidationResponse<Transaction> validate(Transaction model, ValidationResponse<Transaction> response) {
        Date minDate = new Date(2022, 1, 1);
        Date maxDate = new Date(2022, 6, 31);
        if (model.getAccount().getActivationDate().before(minDate) || model.getAccount().getActivationDate().after(maxDate)) {
            response.setMessage("The account activation date is not valid");
        }
        return response;
    }

}
