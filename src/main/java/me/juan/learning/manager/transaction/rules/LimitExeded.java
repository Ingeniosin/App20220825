package me.juan.learning.manager.transaction.rules;

import me.juan.learning.entity.Transaction;
import me.juan.learning.validator.Rule;
import me.juan.learning.validator.ValidationResponse;

public class LimitExeded implements Rule<Transaction> {

    @Override
    public String getName() {
        return "Limit exeded rule";
    }

    @Override
    public ValidationResponse<Transaction> validate(Transaction model, ValidationResponse<Transaction> response) {
        if (model.getAmount() > 20000) {
            response.setMessage("Limit exeded");
        }
        return response;
    }

}
