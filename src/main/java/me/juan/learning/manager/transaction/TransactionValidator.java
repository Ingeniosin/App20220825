package me.juan.learning.manager.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import me.juan.learning.entity.AccountStatus;
import me.juan.learning.entity.Transaction;
import me.juan.learning.legacy.UnusualMotionDetector;
import me.juan.learning.manager.transaction.rules.*;
import me.juan.learning.validator.Rule;
import me.juan.learning.validator.Validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class TransactionValidator implements Validator<Transaction> {

    @Override
    public List<Rule<Transaction>> getRules(Transaction model) {
        return getRulesByConfiguration(ConfigurationType.MAX);
    /*    return List.of(
                new ActiveAccountRule(),
                new ActivationDate(),
                new LimitExeded(),
                new ValidAmountRule(),
                new DateRule(),
                new SufficientFundsRule()
        );*/
    }

    private List<Rule<Transaction>> getRulesByConfiguration(ConfigurationType configurationType){
        List<Rule<Transaction>> rules = new ArrayList<>();
        for (ConfigurationType configuration : ConfigurationType.values()) {
            rules.addAll(configuration.getRules());
            if(configuration.equals(configurationType))
                break;
        }
        return rules;
    }
}

@Getter
@AllArgsConstructor
enum ConfigurationType {
    MIN(List.of(new ActivationDateWithMax(), new AdapterMotionDetectorRule(new UnusualMotionDetector()))),
    MID(List.of(new SufficientFundsRule())),
    MAX(List.of(new LimitExeded()));

    private final List<Rule<Transaction>> rules;
}