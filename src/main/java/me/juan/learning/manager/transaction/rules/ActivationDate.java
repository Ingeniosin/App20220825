package me.juan.learning.manager.transaction.rules;

import me.juan.learning.entity.Transaction;
import me.juan.learning.validator.Rule;
import me.juan.learning.validator.ValidationResponse;

import java.util.Date;

public class ActivationDate implements Rule<Transaction> {

    @Override
    public String getName() {
        return "Activation date rule";
    }

    @Override
    public ValidationResponse<Transaction> validate(Transaction model, ValidationResponse<Transaction> response) {
        Date minDate = new Date(2022, 1, 1);
        if (model.getAccount().getActivationDate().before(minDate)) {
            response.setMessage("The account is not valid");
        }
        return response;
    }

}
