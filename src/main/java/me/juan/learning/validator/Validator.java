package me.juan.learning.validator;

import java.util.List;
import java.util.stream.Collectors;

public interface Validator<T> {

    List<Rule<T>> getRules(T model);

    default List<ValidationResponse<T>> validate(T model) {
        return getRules(model).stream().map(x -> x.validate(model, new ValidationResponse<T>(x))).collect(Collectors.toList());
    }

    default boolean isValid(List<ValidationResponse<T>> validationResponses) {
        return validationResponses.stream().allMatch(ValidationResponse::isValid);
    }


}
