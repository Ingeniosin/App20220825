package me.juan.learning.legacy;

public class UnusualMotionDetector {

    private static final int maxMotion = 1000;

    public String checkMotion(double amount) {
        if (amount > maxMotion) {
            return "Unusual motion detected";
        }
        return null;
    }


}
