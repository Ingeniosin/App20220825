package me.juan.learning.entity;

import lombok.*;
import lombok.experimental.Accessors;
import me.juan.learning.manager.AccountCache;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@ToString
@Data
@Accessors
public class Account {
    private final UUID uuid = UUID.randomUUID();

    @ToString.Exclude
    private User user;

    private double balance;
    private AccountStatus status;
    private AccountType type;

    private Date activationDate;

    public Account(User user, double balance, AccountStatus status, AccountType type, Date activationDate) {
        this.user = user;
        this.balance = balance;
        this.status = status;
        this.type = type;
        this.activationDate = activationDate;
        AccountCache.getInstance().OnCreateAccount(this);
    }

    public Account(User user, double balance, AccountStatus status) {
        this(user, balance, status, AccountType.SAVINGS, new Date());
    }

    public Account(Account account) {
        this(account.getUser(), account.getBalance(), account.getStatus(), account.getType(), account.getActivationDate());
    }

    public boolean isActivated() {
        return status == AccountStatus.ACTIVE && activationDate.before(new Date());
    }

    public Account clone() {
        return new Account(this);
    }

}
