package me.juan.learning.entity;

public enum AccountType {

    SAVINGS,
    CREDIT_CARD,
    CURRENT,

}
