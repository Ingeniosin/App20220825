package me.juan.learning.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class AccountBuilder {

    private User user;
    private double balance;
    private AccountStatus status = AccountStatus.UNDER_REVIEW;
    private AccountType type = AccountType.SAVINGS;
    private Date activationDate;

    public static AccountBuilder getBuilder() {
        return new AccountBuilder();
    }

    public AccountBuilder isSavings() {
        this.type = AccountType.SAVINGS;
        this.status = AccountStatus.ACTIVE;
        this.activationDate = new Date();
        return this;
    }

    public Account build() {
        return new Account(user, balance, status, type, activationDate);
    }



}
