package me.juan.learning;

import me.juan.learning.entity.*;
import me.juan.learning.manager.AccountCache;
import me.juan.learning.manager.procedure.TransactionProcedure;
import me.juan.learning.manager.transaction.TransactionManager;

import java.util.Date;

public class Main {

    public static final TransactionManager transactionManager = new TransactionManager();

    public static void main(String[] args) {
        User user = new User("Juan");

        AccountBuilder builder = AccountBuilder.getBuilder();
        Account userAccount = builder.isSavings().setBalance(2000.0).setUser(user).setActivationDate(new Date(2022, 5, 1)).build();

        Transaction transaction = new Transaction(userAccount, 120);
        transactionManager.beginTransaction(transaction);
        System.out.println(userAccount);


        Account juanAccountClone = AccountCache.getInstance().getCloneAccountByUsername("Juan");

        TransactionProcedure first = new TransactionProcedure(200);
        TransactionProcedure second = new TransactionProcedure(first, 200);
        TransactionProcedure third = new TransactionProcedure(second, 200);
        TransactionProcedure fourth = new TransactionProcedure(third, 200);
        TransactionProcedure fifth = new TransactionProcedure(fourth, 200);

        try {
            System.out.println("Pre procedure: "+juanAccountClone);
            first.run(juanAccountClone);
            System.out.println("Post procedure: "+juanAccountClone);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }


    }

}
